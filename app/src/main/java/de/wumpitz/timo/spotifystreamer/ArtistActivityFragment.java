package de.wumpitz.timo.spotifystreamer;

import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Tracks;
import retrofit.RetrofitError;


/**
 * A placeholder fragment containing a simple view.
 */
public class ArtistActivityFragment extends Fragment {

    private ActionBar actionBar;
    private TrackListAdapter mTrackListAdapter;

    public ArtistActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artist, container, false);
        Intent intent = getActivity().getIntent();

        // Get the ActionBar
        if (actionBar == null) {
            actionBar = ((ArtistActivity) getActivity()).getSupportActionBar();
        }

        if (intent != null && intent.hasExtra(Intent.EXTRA_TEXT)) {
            String artistId = intent.getStringArrayExtra(Intent.EXTRA_TEXT)[0];
            String artistName = intent.getStringArrayExtra(Intent.EXTRA_TEXT)[1];


            // Customize the ActionBar
            if (actionBar != null) {
                actionBar.setSubtitle(artistName);
            }

            // Initialise the track ListView
            mTrackListAdapter = new TrackListAdapter(getActivity());
            ListView listContainer = (ListView) rootView.findViewById(R.id.listview_top_tracks);
            listContainer.setAdapter(mTrackListAdapter);

            // Start the task for fetching the top tracks
            new FetchTopTracksTask().execute(artistId);
        }
        return rootView;
    }

    private class FetchTopTracksTask extends AsyncTask<String, Void, Tracks> {

        private final String LOG_TAG = FetchTopTracksTask.class.getSimpleName();

        protected Tracks doInBackground(String... artistId) {
            // Initialise Spotify API and call the top tracks endpoint
            SpotifyApi api = new SpotifyApi();
            SpotifyService spotify = api.getService();
            HashMap options = new HashMap<String, String>();
            // TODO Remove hardcoded country
            options.put("country", "DE");
            try {
                return spotify.getArtistTopTrack(artistId[0], options);
            } catch (RetrofitError e) {
                return null;
            }
        }
        protected void onPostExecute(Tracks tracks) {
            if (tracks == null) {
                Toast.makeText(getActivity(), "A network problem occured. Please retry.", Toast.LENGTH_SHORT).show();
            }
            Log.v(LOG_TAG, "Found " + tracks.tracks.toArray().length + " tracks.");
            mTrackListAdapter.clear();
            // Add the tracks to the adapter if any. Otherwise inform the user
            if (tracks.tracks.toArray().length > 0) {
                mTrackListAdapter.addAll(tracks.tracks);
            } else {
                Toast.makeText(getActivity(), "No tracks found.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
