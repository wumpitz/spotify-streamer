package de.wumpitz.timo.spotifystreamer;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kaaes.spotify.webapi.android.models.Artist;
import kaaes.spotify.webapi.android.models.Image;


public class ArtistListAdapter extends ArrayAdapter<Artist>{
    private final Activity context;

    public ArtistListAdapter(Activity context) {
        super(context, R.layout.list_item_artist);
        this.context = context;
    }

    /**
     * Find the smallest image based on the width in the given
     * list of images.
     *
     * @param images A list of Images
     * @return The smallest image based on the width
     */
    private Image getSmallestImage(List<Image> images) {
        Image result = null;
        for (Image image : images) {
            if (result == null || image.width < result.width) {
                result = image;
            }
        }
        return result;
    }

    @Override
    public View getView(int position, View recycled, ViewGroup parent) {
        if (recycled == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            recycled = inflater.inflate(R.layout.list_item_artist, parent, false);
        }
        // Get the artist and the smallest image for it
        Artist artist = getItem(position);
        Image image = getSmallestImage(artist.images);

        ImageView imageView = (ImageView) recycled.findViewById(R.id.list_item_artist_imageview);
        TextView artistView = (TextView) recycled.findViewById(R.id.list_item_artist_textview);

        artistView.setText(artist.name);

        // If there's an image for the artist insert it into the ImageView using
        // Glide
        if (image != null) {
            Glide.with(context)
                    .load(image.url)
                    .centerCrop()
                    .crossFade()
                    .into(imageView);
        }
        return recycled;
    }
}
