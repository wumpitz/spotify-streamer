package de.wumpitz.timo.spotifystreamer;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kaaes.spotify.webapi.android.models.Image;
import kaaes.spotify.webapi.android.models.Track;


public class TrackListAdapter extends ArrayAdapter<Track>{
    private final Activity context;

    public TrackListAdapter(Activity context) {
        super(context, R.layout.list_item_track);
        this.context = context;
    }

    /**
     * Find the smallest image based on the width in the given
     * list of images.
     *
     * @param images A list of Images
     * @return The smallest image based on the width
     */
    private Image getSmallestImage(List<Image> images) {
        Image result = null;
        for (Image image : images) {
            if (result == null || image.width < result.width) {
                result = image;
            }
        }
        return result;
    }

    @Override
    public View getView(int position, View recycled, ViewGroup parent) {
        if (recycled == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            recycled = inflater.inflate(R.layout.list_item_track, parent, false);
        }
        // Get the artist and the smallest image for it
        Track track = getItem(position);
        Image image = getSmallestImage(track.album.images);

        ImageView imageView = (ImageView) recycled.findViewById(R.id.list_item_album_imageview);
        TextView albumNameView = (TextView) recycled.findViewById(R.id.list_item_albumname_textview);
        TextView trackNameView = (TextView) recycled.findViewById(R.id.list_item_trackname_textview);

        albumNameView.setText(track.album.name);
        trackNameView.setText(track.name);

        // If there's an image for the artist insert it into the ImageView using
        // Glide
        if (image != null) {
            Glide.with(context)
                    .load(image.url)
                    .centerCrop()
                    .crossFade()
                    .into(imageView);
        }
        return recycled;
    }
}
