package de.wumpitz.timo.spotifystreamer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Artist;
import kaaes.spotify.webapi.android.models.ArtistsPager;
import retrofit.RetrofitError;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private final String LOG_TAG = MainActivityFragment.class.getSimpleName();
    private final Integer START_SEARCH_AT = 3;

    private EditText searchText;
    private ArtistListAdapter mArtistAdapter;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        // Create the ListAdapter for the artists and bind it to the ListView
        mArtistAdapter = new ArtistListAdapter(getActivity());
        ListView listContainer = (ListView) rootView.findViewById(R.id.listview_search_result);
        listContainer.setAdapter(mArtistAdapter);

        // Add a ClickListener for the items to start the ArtistActivity
        listContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist artist = (Artist) parent.getItemAtPosition(position);
                // Add the artist ID and name to the Intent
                Intent detailIntent = new Intent(getActivity(), ArtistActivity.class)
                        .putExtra(Intent.EXTRA_TEXT, new String[]{artist.id, artist.name});
                startActivity(detailIntent);
            }
        });

        // Initialise search input
        searchText = (EditText) rootView.findViewById(R.id.input_search);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                Log.v(LOG_TAG, "User input: " + s.toString());
                if (s.length() >= START_SEARCH_AT) {
                    // Start Search Task
                    new PerformSearchTask().execute(s.toString());
                }
            }
        });
        return rootView;
    }

    private class PerformSearchTask extends AsyncTask<String, Void, ArrayList<Artist>> {

        private final String LOG_TAG = PerformSearchTask.class.getSimpleName();
        private final Integer SEARCH_RESULT_COUNT = 10;

        protected ArrayList<Artist> doInBackground(String... searchText) {
            if (searchText.length == 0) {
                return null;
            }
            Log.v(LOG_TAG, "Start search for " + searchText[0]);

            // Initialise Spotify API
            SpotifyApi api = new SpotifyApi();
            SpotifyService spotify = api.getService();

            // Start search for artists
            ArtistsPager artistsPager;
            try {
                artistsPager = spotify.searchArtists(searchText[0]);
            } catch (RetrofitError e) {
                return null;
            }
            Integer i = 0;
            ArrayList<Artist> artistResult = new ArrayList<>();
            for (Iterator<Artist> iter = artistsPager.artists.items.iterator();
                 iter.hasNext() && i < SEARCH_RESULT_COUNT; ) {
                Artist artist = iter.next();
                artistResult.add(artist);
                Log.d(LOG_TAG, "Found " + artist.name);
                i++;
            }
            return artistResult;
        }

        protected void onPostExecute(ArrayList<Artist> result) {
            if (result == null) {
                Toast.makeText(getActivity(), "A network problem occured. Please retry.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (result.toArray().length == 0) {
                Toast.makeText(getActivity(), "No artist found.", Toast.LENGTH_SHORT).show();
                mArtistAdapter.clear();
                return;
            }
            Log.d(LOG_TAG, "Search result count: " + result.toArray().length);
            mArtistAdapter.clear();
            mArtistAdapter.addAll(result);
        }
    }
}
